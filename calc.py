# Programa inicial en repositorio git -> calculadora simple

def suma(numero1, numero2):
    return numero1 + numero2

def resta(numero1, numero2):
    return numero1 - numero2

print("Suma 1: 1 + 2 = ", suma(1,2))
print("Suma 2: 3 + 4 = ", suma(3,4))

print("Resta 1: 6 - 5 = ", resta(6,5))
print("Resta 2: 8 - 7 = ", resta(8,7))
